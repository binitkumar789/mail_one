<?php

namespace App\Http\Controllers;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail()
    {
        $details=[
            'title'=>'mail from singh',
            'body'=>'This is testing one',
            'payload' => 'abhiseksingh875@gmail.com'
        ];

        Mail::to("abhiseksingh875@gmail.com")->cc("abhisheksingh.debut@gmail.com")->send(new TestMail( $details));
        return "Mail sent";
    }
}
